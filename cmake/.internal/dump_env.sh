#!/bin/bash
intdir=`dirname $0`
intdir=`cd $intdir; pwd`

# Utility function: contains(list element)
contains() {
  for item in $1; do if [ "$item" == "$2" ]; then return 0; fi; done
  return 1
}

# Check if BINARY_TAG is set (do not take it from CMTCONFIG)
if [ "$BINARY_TAG" == "" ]; then
  echo "ERROR! BINARY_TAG is not set"
  exit 1
fi

# Get build and install directory location
# [BUILDDIR can no longer be set from CORALCOOL_CMAKE_BUILDDIR (CORALCOOL-2846)]
cd $intdir/../..
blddir=build.${BINARY_TAG}
insdir=${BINARY_TAG}
###if [ "$CORALCOOL_CMAKE_BUILDDIR" != "" ]; then
###  blddir=$CORALCOOL_CMAKE_BUILDDIR
###fi
if [ ! -d $blddir ]; then
  echo "ERROR! Directory $blddir does not exist in `pwd`"
  exit 1
fi
blddir=`cd $blddir; pwd`
if [ ! -d $insdir ]; then
  echo "ERROR! Directory $insdir does not exist in `pwd`"
  exit 1
fi
insdir=`cd $insdir; pwd`

# Check arguments
if [ "$1" == "-c" ] || [ "$1" == "--comparetocmt" ]; then
  c2cmt=1
  shift
else
  c2cmt=0
fi
if [ "$1" == "-i" ] || [ "$1" == "--install" ]; then
  xenv=install
  shift
  envdir=$insdir
elif [ "$1" == "" ]; then
  xenv=build
  envdir=$blddir
else
  echo "Usage: $0 [-c|--comparetocmt] [-i|--install]"
  exit 1
fi

# Dump runtime environments to files
# Use "env -i" to start with a fresh environment
# See http://unix.stackexchange.com/questions/48994
cd $intdir
if [ $c2cmt -eq 1 ]; then
  env -i CMTCONFIG=$BINARY_TAG BINARY_TAG=$BINARY_TAG ./cc-run-cmt env | sort > envCMT
else
  \rm -f envCMT; touch envCMT
fi
env -i PATH=`env -i bash --norc --noprofile -c 'echo $PATH'` USER=$USER ${envdir}/cc-run env | sort > envCMake.${xenv}

# Read runtime environments from files and compare them
files="envCMT envCMake.${xenv}"
envs=`cat $files | awk '{nf=split($0,a,"="); if (nf<2){print "ERROR! No \"=\" found in \""$0"\"" > "/dev/stderr"; exit 1}; if(index(a[1]," ")>0){print "ERROR! Blank space found in \""a[1]"\"" > "/dev/stderr"; exit 1}; print a[1]}'`
if [ "$?" != "0" ]; then
  echo "ERROR! Unexpected input in $files"
  exit 1
fi
envs=`echo $envs | tr " " "\n" | sort -u`
envs_skip="_ AFS ATLAS_TAGS_MAP CCACHE_DIR CLASSPATH CMAKEFLAGS CMAKE_PREFIX_PATH CMTBIN CMTPATH CMTPROJECTPATH CMTROOT CMTVERS COMPILER_PATH host_cmtconfig NEWCMTCONFIG OLDPWD QM_home SHLVL SITEROOT USER"
if ! contains "$envs" "COOLSYS"; then
  envs_skip="$envs_skip ROOT_INCLUDE_PATH"
fi
for env in $envs; do
  if contains "$envs_skip" "$env"; then continue; fi
  # Workaround for XXXROOT and XXXCONFIG appearing if CMTUSERCONTEXT is set
  if [ "${env}" != "${env%ROOT}" ]; then continue; fi
  if [ "${env}" != "${env%CONFIG}" ] && [ "${env}" != "CMTCONFIG" ]; then continue; fi
  # Read runtime environments from files
  cmk=""
  for file in $files; do
    cmt=$cmk
    cmk=`cat $file | grep ^${env}=`
    if [ "$cmk" == "" ]; then
      cmk="[none]"
    else
      cmk=`echo $cmk | sed "s/^${env}=//" | sed "s|/:|:|g"`
    fi
  done
  if [ $c2cmt -eq 1 ]; then
    echo "=== Compare $env"
  else
    echo "=== Dump $env"
  fi
  # Compare cvmfs paths even if CMT uses afs (workaround for SPI-912)
  cmt=`echo "$cmt" | sed 's|/afs/cern.ch/sw/lcg/releases|/cvmfs/sft.cern.ch/lcg/releases|g'`; cmt=${cmt%:}
  # Use simpler expressions for equivalent paths (and remove trailing ":")
  cmt=`echo "$cmt" | tr ":" "\n" | sed 's|releases/LCGCMT/LCGCMT_..root6/LCG_Settings/\.\./\.\./\.\.|releases|g' | tr "\n" ":"`; cmt=${cmt%:}
  cmt=`echo "$cmt" | tr ":" "\n" | sed 's|releases/LCGCMT/LCGCMT_.../LCG_Settings/\.\./\.\./\.\.|releases|g' | tr "\n" ":"`; cmt=${cmt%:}
  cmt=`echo "$cmt" | tr ":" "\n" | sed 's|releases/LCGCMT/LCGCMT_../LCG_Settings/\.\./\.\./\.\.|releases|g' | tr "\n" ":"`; cmt=${cmt%:}
  cmt=`echo "$cmt" | tr ":" "\n" | sed 's|releases/LCGCMT/LCGCMT_86-d10d6/LCG_Settings/\.\./\.\./\.\.|releases/LCG_86|g' | tr "\n" ":"`; cmt=${cmt%:} # AD-HOC BUG FIX...
  cmt=`echo "$cmt" | tr ":" "\n" | sed 's|/src/../|/|g' | sed "s|$BINARY_TAG/../$BINARY_TAG|$BINARY_TAG|g" | tr "\n" ":"`; cmt=${cmt%:}
  # Do not hardcode the specific BINARY_TAG in path dumps
  cmt=`echo "$cmt" | sed 's|'$BINARY_TAG'/|${BINARY_TAG}/|g'`; cmt=${cmt%:}
  cmk=`echo "$cmk" | sed 's|'$BINARY_TAG'/|${BINARY_TAG}/|g'`; cmk=${cmk%:}
  cmt=`echo "$cmt" | sed 's|/'$BINARY_TAG'|/${BINARY_TAG}|g'`; cmt=${cmt%:}
  cmk=`echo "$cmk" | sed 's|/'$BINARY_TAG'|/${BINARY_TAG}|g'`; cmk=${cmk%:}
  # Compare the two environments and dump them if they differ
  if [ "$cmt" != "$cmk" ]; then
    if [ ! $c2cmt -eq 1 ]; then
      echo $cmk | tr ":" "\n"; echo; continue
    fi
    cmtls=`echo $cmt | tr ":" "\n" | sort -u`
    cmkls=`echo $cmk | tr ":" "\n" | sort -u`
    allls=`echo $cmt:$cmk | tr ":" "\n" | sort -u`
    if [ "$cmt" == "" ]; then cmt="[empty]"; fi
    if [ "$cmk" == "" ]; then cmk="[empty]"; fi
    echo $cmt | tr ":" "\n"; echo; echo $cmk | tr ":" "\n"    
    if [ "$cmt" == "[empty]" ] || [ "$cmk" == "[empty]" ]; then continue; fi
    if [[ `echo $cmtls | wc -w` > 1 ]] || [[ `echo $cmkls | wc -w` > 1 ]]; then
      echo "---"
      for ia in $allls; do
        if contains "$cmtls" "$ia"; then
          if ! contains "$cmkls" "$ia"; then echo "- $ia"; fi
        else
          echo "+ $ia"
        fi        
      done
    fi
  else
    echo OK
  fi
done
\rm -f $files

# Notes for debugging:
# 1. To know where a variable is defined in CMT, use
# "env -i CMTCONFIG=$CMTCONFIG BINARY_TAG=$BINARY_TAG ../../../.internal/cc-run-cmt bash --norc"
# and then the usual "cmt show set <variable>".
# 2. To know where a variable is defined in CMake, 
# check first for _all_ CMakeLists.txt in CORAL packages, otherwise use
# "./cc-cacherebuild --trace" (this is very verbose!).
