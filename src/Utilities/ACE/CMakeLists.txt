# Required external packages
find_package(CORAL REQUIRED)
include_directories(${CORAL_INCLUDE_DIRS})

# ACE is not supported on MacOSX (CORALCOOL-2882)
IF(NOT CMAKE_HOST_SYSTEM_NAME MATCHES "Darwin")

  find_package(Qt4) # Really REQUIRED, but avoid blocking (CORALCOOL-2813)

  if (QT4_FOUND) # Do not build ACE if Qt is not found (CORALCOOL-2813)

    IF(BINARY_TAG MATCHES "clang") # CORALCOOL-2870
      set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-self-assign")
    ENDIF()

    include(${QT_USE_FILE})

    file(GLOB ACE_moc_hdrs ${CMAKE_CURRENT_SOURCE_DIR}/ACE/*.h)
    QT4_WRAP_CPP(ACE_moc_srcs ${ACE_moc_hdrs})
    include_directories(${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_BINARY_DIR})

    file(GLOB ACE_srcs ${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp) 
    add_executable(ace ${ACE_srcs} ${ACE_moc_srcs})
    target_link_libraries(ace ${QT_LIBRARIES} lcg_CoolApplication)
    install(TARGETS ace DESTINATION bin)

    coral_build_and_release_env(PREPEND PATH ${QT_BINARY_DIR})
    coral_build_and_release_env(PREPEND LD_LIBRARY_PATH ${QT_LIBRARY_DIR})
    get_filename_component(QTDIR ${QT_BINARY_DIR} PATH)
    coral_build_and_release_env(SET QTDIR ${QTDIR})

  endif()

ELSE()

  file(GLOB ACE_srcs ${CMAKE_CURRENT_SOURCE_DIR}/src/MainWindow.cpp) 
  add_executable(ace ${ACE_srcs})
  target_link_libraries(ace lcg_CoolApplication)
  install(TARGETS ace DESTINATION bin)

ENDIF()

add_dependencies(ace PRE_BUILD_BANNER)
add_dependencies(POST_BUILD_BANNER ace)


