# Required external packages
find_package(CORAL REQUIRED)
include_directories(${CORAL_INCLUDE_DIRS})

include(CORALConfigUtilities)
coral_add_utility(coolDBDiscovery SRCDIR src
                  LIBS lcg_CoolKernel ${CORAL_RelationalAccess_LIBRARY})
 
