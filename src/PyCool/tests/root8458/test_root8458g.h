#ifndef TEST_ROOT8458G_H
#define TEST_ROOT8458G_H 1

#include <iostream> // Debug ROOT-8458
#include "test_root8458g_lib.h"

namespace rbug
{
  // IRecordSelection interface
  class IRecordSelection4
  {
  public:
    virtual ~IRecordSelection4() {}
    virtual bool canSelect( const IRecordSpecification4& spec ) const = 0;
    virtual bool select( const IRecord4& record ) const = 0;
    virtual IRecordSelection4* clone() const = 0;
  private:
    IRecordSelection4& operator=( const IRecordSelection4& rhs );
  };

  // FieldSelection class
  class FieldSelection4 : virtual public IRecordSelection4
  {
  public:
    enum Relation { EQ, NE, GT, GE, LT, LE };
    enum Nullness { IS_NULL, IS_NOT_NULL };
    virtual ~FieldSelection4(){}
    template<typename T> FieldSelection4( const std::string& name,
                                          const StorageType4::TypeId typeId,
                                          Relation relation,
                                          const T& refValue );
    // IRecordSelection interface
    bool canSelect( const IRecordSpecification4& ) const{ return true; }
    bool select( const IRecord4& ) const{ return true; }
    IRecordSelection4* clone() const{ return nullptr; }
  private:
    FieldSelection4();
    FieldSelection4( const FieldSelection4& rhs );
    FieldSelection4& operator=( const FieldSelection4& rhs );
  private:
    Record4 m_refValue;
    Relation m_relation;
  };

  //---------------------------------------------------------------------------

  template<typename T>
  inline FieldSelection4::FieldSelection4( const std::string& name,
                                           const StorageType4::TypeId typeId,
                                           Relation relation,
                                           const T& refValue )
    : m_refValue( FieldSpecification4( name, typeId ) )
    , m_relation( relation )
  {
#ifndef CRASH1
#pragma message("MAY NOT CRASH (add a printout)")
    std::cout << "FieldSelection for StorageType=" << typeId << "(" << StorageType4::storageType(typeId).name() << "," << StorageType4::storageType(typeId).cppType().name() << ") against " << name << "(" << typeid(T).name() << ")" << std::endl;
#else
#pragma message("WILL CRASH (do not add a printout)")
#endif
    m_refValue[0].setValue( refValue );
  }

  //---------------------------------------------------------------------------

  // Template instantiation
  template rbug::FieldSelection4::FieldSelection4( const std::string& name, const StorageType4::TypeId typeId, Relation relation, const rbug::Bool& b );
  template rbug::FieldSelection4::FieldSelection4( const std::string& name, const StorageType4::TypeId typeId, Relation relation, const rbug::UChar& h );
  template rbug::FieldSelection4::FieldSelection4( const std::string& name, const StorageType4::TypeId typeId, Relation relation, const rbug::Int16& s );
  template rbug::FieldSelection4::FieldSelection4( const std::string& name, const StorageType4::TypeId typeId, Relation relation, const rbug::UInt16& t );
  template rbug::FieldSelection4::FieldSelection4( const std::string& name, const StorageType4::TypeId typeId, Relation relation, const rbug::Int32& i );
  template rbug::FieldSelection4::FieldSelection4( const std::string& name, const StorageType4::TypeId typeId, Relation relation, const rbug::UInt32& j );
  template rbug::FieldSelection4::FieldSelection4( const std::string& name, const StorageType4::TypeId typeId, Relation relation, const rbug::Int64& x );
  template rbug::FieldSelection4::FieldSelection4( const std::string& name, const StorageType4::TypeId typeId, Relation relation, const rbug::UInt64& y );
  template rbug::FieldSelection4::FieldSelection4( const std::string& name, const StorageType4::TypeId typeId, Relation relation, const rbug::Float& f );
  //=== WHEN a cout is included in FieldSelectil ctor, THEN:
  // - including this below causes this test NOT to throw and abort
  // - commenting out this line below causes this test to throw and abort
#ifndef CRASH2
#pragma message("MAY NOT CRASH (add a template)")
  template rbug::FieldSelection4::FieldSelection4( const std::string& name, const StorageType4::TypeId typeId, Relation relation, const rbug::Double& d );
#else
#pragma message("WILL CRASH (do not add a template)")
#endif
  //=== These three below are always irrelevant and I keep them out...
  //template rbug::FieldSelection4::FieldSelection4( const std::string& name, const StorageType4::TypeId typeId, Relation relation, const rbug::String255& Ss );
  //template rbug::FieldSelection4::FieldSelection4( const std::string& name, const StorageType4::TypeId typeId, Relation relation, const rbug::Blob64k& N5coral4BlobE );

}
#endif // TEST_ROOT8458G_H

