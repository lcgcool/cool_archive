# See also test_root6068b.py (and CORALCOOL-2959)
import sys
if len(sys.argv)!=2 or ( sys.argv[1]!='crash' and sys.argv[1]!='nocrash' ):
    # NB: 'crash' causes a crash, 'nocrash' does not cause a crash
    print "Usage: python", sys.argv[0], "<crash|nocrash>"
    sys.exit(0)
if sys.argv[1]=='crash': crash=True
else: crash=False
print "Your choice: CRASH =",crash

# Import mini-PyCool
import os
dir=os.path.dirname( os.path.realpath(__file__) )
import cppyy
###cppyy.gbl.gSystem.Load('liblcg_CoolApplication.so')
cppyy.gbl.gSystem.Load('liblcg_CoolKernel.so')
if crash: cppyy.gbl.gInterpreter.ProcessLine("#define CRASH 1")
cppyy.gbl.gInterpreter.ProcessLine('#include "'+dir+'/test_root8458e.h"')
cool  = cppyy.gbl.cool

# Test (as in test_root6068.py, unstable vs crash/nocrash)
cool.FieldSelection4("i",cool.StorageType.Int32,cool.FieldSelection4.EQ,10)

# Test (as in the original test_root8458.py, no instability observed)
#cool.FieldSelection4("x",cool.StorageType.Bool,cool.FieldSelection4.EQ,True)
#cool.FieldSelection4("x",cool.StorageType.Bool,cool.FieldSelection4.EQ,10)
#cool.FieldSelection4("x",cool.StorageType.Bool,cool.FieldSelection4.EQ,10.)
