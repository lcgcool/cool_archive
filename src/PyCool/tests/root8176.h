#include <memory>
#include <iostream>
class IInt {
public:
  virtual ~IInt(){}
  virtual int getint() = 0;
};
class Int : virtual public IInt {
public:
  virtual ~Int(){}
  Int( int i ) : m_i(i) {}
  int getint() { return m_i; };
private:
  int m_i;
  Int();
};
typedef std::shared_ptr<IInt> IIntPtr;
class root8176 {
public:
  IIntPtr handle( int i )
  {
    std::shared_ptr<Int> spi( new Int( i ) );
    return spi;
  }
  void printpi( IInt* pi )
  {
    std::cout << pi->getint() << std::endl;
  }
};
