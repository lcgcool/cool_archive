package CoolKernel

#============================================================================
# Public dependencies and build rules
#============================================================================

# Define CMTINSTALLAREA correctly
use LCG_Policy v*

use Boost v* LCG_Interfaces
use CORAL v* LCG_Interfaces

# This is only needed to determine if c++11 (ROOT6) should be used
###use ROOT v* LCG_Interfaces -no_auto_imports
###macro_append cppflags " -I$(ROOT_home)/include "

#----------------------------------------------------------------------------
# Library
#----------------------------------------------------------------------------

apply_pattern include_dir_policy
apply_pattern lcg_shared_library

#----------------------------------------------------------------------------
# COOL400 API changes (manual transaction management task #2200)
#----------------------------------------------------------------------------

# Uncomment this line to enable the COOL400 API extensions
# [NB: COOL290 extension control is fully in VersionInfo.h (bug #92204)]
###macro_append use_cppflags ' -DCOOL400' target-winxp ' /DCOOL400'
###macro_append gccxmlopts ' -DCOOL400'

#----------------------------------------------------------------------------
# Compilation and link flags (inherited by all COOL packages)
#----------------------------------------------------------------------------

# Workaround for undefined __int128 in Coverity builds (SPI-343)
###macro_remove cppflags "-std=c++0x"
###macro_remove cppflags "-std=c++11"

# Compile with pedantic warnings (CORALCOOL-2804)
macro_append cppflags ' -pedantic '

# --- Configure Windows build options ---
# Workaround for CMT bug #46458 (make all_groups on vc9)
macro_remove cmt_actions_constituents "" target-winxp "make"

# Disable CRT deprecation warnings on WIN (getenv, sprintf...)
macro_append use_cppflags '' target-winxp ' /D_CRT_SECURE_NO_WARNINGS'

# Disable SCL deprecation warnings from boost headers on WIN (bug #76887)
macro_append use_cppflags '' target-winxp ' /D_SCL_SECURE_NO_WARNINGS'

# Enable Boost lib diagnostic on WIN (see boost/config/auto_link.hpp)
macro_append use_cppflags '' target-winxp ' /DBOOST_LIB_DIAGNOSTIC'

# Disable automatic selection of Boost libraries to link against on WIN
# (this is needed if you upgrade to Boost 1.35 but use its vc71 build)
macro_append use_cppflags '' target-winxp ' /DBOOST_ALL_NO_LIB'

# Attempt to catch C++ exceptions as well as WIN32 structured exceptions
# See http://http://msdn.microsoft.com/en-us/library/1deeycx5(VS.80).aspx
# The /EHa flag is mandatory (on vc7 and vc9) if _set_se_translator is used 
# See http://msdn.microsoft.com/en-us/library/5z4bw5h5(VS.80).aspx
macro_remove cppflags '' target-winxp ' /EHsc'
macro_append cppflags '' target-winxp ' /EHa'

# --- Configure icc build options ---
# Fix linker warning 'feupdateenv is not implemented and will always fail'
# See http://www-lab.imr.edu/~sgi/new/intel/cc9.1/Release_Notes.htm
macro_append cpplinkflags '' target-icc ' -i-dynamic '
macro_append clinkflags '' target-icc ' -i-dynamic '
# Remove invalid compiler option '-W'
macro_remove cppflags '' target-icc '-W '
# Add leading blank before appending options (avoid issues like bug #83825)
macro_append cppflags '' target-icc ' '
# Disable remark #193: zero used for undefined preprocessing identifier
macro_append cppflags '' target-icc '-wd193 '
# Disable remark #304: access control not specified ("public" by default)
macro_append cppflags '' target-icc '-wd304 '
# Disable remark #383: valued copied to temporary, reference to temporary
macro_append cppflags '' target-icc '-wd383 '
# Disable remark #981: operands are evaluated in unspecified order
macro_append cppflags '' target-icc '-wd981 '
# Disable remark #1418: external function definition with no prior declaration
macro_append cppflags '' target-icc '-wd1418 '
# Disable warning #1478: class std::auto_ptr was declared deprecated
macro_append cppflags '' target-icc '-wd1478 '
# Disable remark #1572: floating-point equality comparisons unreliable
macro_append cppflags '' target-icc '-wd1572 '
# Disable remark #2259: "int" to "unsigned short" may lose significant bits
# [would prefer to keep it, but there's too many in Boost headers!]
macro_append cppflags '' target-icc '-wd2259 '
# Keep remark #68: integer conversion resulted in a change of sign
macro_append cppflags '' target-icc '-ww68 '
# Keep remark #82: storage class is not first
macro_append cppflags '' target-icc '-ww82 '
# Keep remark #111: statement is unreachable
macro_append cppflags '' target-icc '-ww111 '
# Keep remark #128: loop is not reachable from preceding code
macro_append cppflags '' target-icc '-ww128 '
# Keep remark #177: variable "xxx" was declared but never referenced
macro_append cppflags '' target-icc '-ww177 '
# Keep remark #181: argument incompatible with format string conversion
macro_append cppflags '' target-icc '-ww181 '
# Keep warning #191: type qualifier is meaningless on cast type
# Keep warning #279: controlling expression is constant
# Keep warning #327: NULL reference is not allowed
# Keep remark #424: extra ";" ignored
macro_append cppflags '' target-icc '-ww424 '
# Keep remark #444: destructor for base class "xxx" is not virtual
macro_append cppflags '' target-icc '-ww444 '
# Keep remark #522: function redeclared "inline" after being called
macro_append cppflags '' target-icc '-ww522 '
# Keep remark #593: variable "xxx" was set but never used
macro_append cppflags '' target-icc '-ww593 '
# Keep warning #654: overloaded virtual function is only partially overridden
# Keep warning #810: conversion may lose significant bits
# Keep remark #1419: external declaration in primary source file
macro_append cppflags '' target-icc '-ww1419 '
# Keep remark #1599: declaration hides variable "xxx" (declared at line nnn)
macro_append cppflags '' target-icc '-ww1599 '
# Keep warning #2165: declaring a reference with "mutable" is nonstandard

#----------------------------------------------------------------------------
# Boost additional libraries as in CoralBase (see also bug #96081)
#----------------------------------------------------------------------------

macro_append CoolKernel_linkopts ' $(Boost_linkopts)'
macro_append lcg_CoolKernel_shlibflags ' $(Boost_linkopts)'

macro_append CoolKernel_linkopts ' $(Boost_linkopts_filesystem)'
macro_append lcg_CoolKernel_shlibflags ' $(Boost_linkopts_filesystem)'

macro_append CoolKernel_linkopts ' $(Boost_linkopts_date_time)'
macro_append lcg_CoolKernel_shlibflags ' $(Boost_linkopts_date_time)'

macro_append CoolKernel_linkopts ' $(Boost_linkopts_system)'
macro_append lcg_CoolKernel_shlibflags ' $(Boost_linkopts_system)'

macro_append CoolKernel_linkopts ' $(Boost_linkopts_thread)'
macro_append lcg_CoolKernel_shlibflags ' $(Boost_linkopts_thread)'

#----------------------------------------------------------------------------
# Prototype changes in LCG_Policy requirements (SPI-169)
#----------------------------------------------------------------------------

# Keep pattern lcg_cond_mkdir unchanged
pattern lcg_cond_mkdir \
  action lcg_mkdir "if [ ! -d <dir> ]; then mkdir -p <dir>; fi" \
    target-winxp 'if not exist "<dir>" mkdir "<dir>"'

# Redefine pattern lcg_cond_mkdir: add <category> and <name>, remove <tstexp>
pattern lcg_cond_mkdir_with_dep \
  apply_pattern lcg_cond_mkdir dir=<dir> ; \
  macro_append <category>_constituents "lcg_mkdir " ; \
  macro <name>_dependencies "lcg_mkdir" 

# Add new pattern lcg_application_template
# With respect to Benedikt's: replace <category>s by <category>
# With respect to Benedikt's: replace suffix=<name> by suffix=_<appname> (2x)
# With respect to Benedikt's: do not use utilities/bin and utilities/lib
pattern lcg_application_template \
  application <appname> -group=<category> -suffix=_<appname> -import=<import> -import=<import2> <files> bindirname=<bindirname> ;\
  macro_append <appname>linkopts " $(<package>_linkopts) $(<package>_<category>_linkopts) $(gcov_linkopts) $(icc_linkopts) " \
    target-winxp " $(<package>_linkopts) $(<package>_<category>_linkopts) "

# Add new pattern lcg_application (bindirname=bin)
pattern lcg_application \
  apply_pattern lcg_application_template appname=<appname> import=<import> import2=<import2> files=<files> category=<category> bindirname=bin

# Redefine pattern lcg_tstexp_application (bindirname=<tstexp>s/bin)
pattern lcg_tstexp_application \
  apply_pattern lcg_cond_mkdir_with_dep dir=$(CMTINSTALLAREA)/$(CMTCONFIG)/<tstexp>s/lib category=<tstexp>s name=<tstexp>_<name> ;\
  apply_pattern lcg_application_template appname=<tstexp>_<name> import=<import> import2=<import2> files=<files> category=<tstexp>s bindirname=<tstexp>s/bin ;\
  macro_append <tstexp>_<name>linkopts " -L$(CMTINSTALLAREA)/$(CMTCONFIG)/<tstexp>s/lib " target-winxp " /LIBPATH:$(CMTINSTALLAREA)/$(CMTCONFIG)/<tstexp>s/lib "

# Redefine pattern lcg_tstexp_library
pattern lcg_tstexp_library \
  apply_pattern lcg_cond_mkdir_with_dep dir=$(CMTINSTALLAREA)/$(CMTCONFIG)/<category>/lib category=<tstexp>s name=<tstexp>_<name> ;\
  library <tstexp>_<name> -group=<tstexp>s -suffix=<name> <files> libdirname="<tstexp>s/lib" ;\
  macro <tstexp>_<name>_shlibflags "$(libraryshr_linkopts) $(<package>_linkopts) -L$(CMTINSTALLAREA)/$(CMTCONFIG)/<tstexp>s/lib $(use_linkopts) $(gcov_linkopts) $(icc_linkopts) " \
    target-winxp "$(libraryshr_linkopts) $(<package>_linkopts) /LIBPATH:$(CMTINSTALLAREA)/$(CMTCONFIG)/<tstexp>s/lib $(use_linkopts) " ;\
  macro_append <package>_<tstexp>s_linkopts " -l<tstexp>_<name> " target-winxp " <tstexp>_<name>.lib " 

# Redefine pattern lcg_tstexp_module
pattern lcg_tstexp_module \
  apply_pattern lcg_cond_mkdir_with_dep dir=$(CMTINSTALLAREA)/$(CMTCONFIG)/<tstexp>s/lib category=<tstexp>s name=<tstexp>_<name> ;\
  library   <tstexp>_<name>   -group=<tstexp>s -suffix=<name> <files> libdirname="<tstexp>s/lib" ;\
  macro lib_<tstexp>_<name>_cppflags "" target-winxp "-DPLUGIN_MANAGER_SAMPLE_BUILD_DLL" ;\
  macro <tstexp>_<name>_shlibflags "$(libraryshr_linkopts) $(<package>_linkopts) -L$(CMTINSTALLAREA)/$(CMTCONFIG)/<tstexp>s/lib $(<package>_<tstexp>s_linkopts) $(use_linkopts) " \
                      target-winxp "$(libraryshr_linkopts) $(<package>_linkopts) /LIBPATH:$(CMTINSTALLAREA)/$(CMTCONFIG)/<tstexp>s/lib $(<package>_<tstexp>s_linkopts) $(use_linkopts) "

# Redefine pattern lcg_test_application
pattern lcg_test_application \
  apply_pattern lcg_tstexp_application name=<name> files=<files> tstexp=test import=<import> import2=<import2>

# Redefine pattern lcg_example_application
pattern lcg_example_application \
  apply_pattern lcg_tstexp_application name=<name> files=<files> tstexp=example

# Remove pattern lcg_unit_test_application
pattern lcg_unit_test_application ""

#----------------------------------------------------------------------------
# Define the pattern for unit tests in COOL
#----------------------------------------------------------------------------

pattern cool_unit_test \
  apply_pattern lcg_test_application name=<package>_<tname> files=../tests/<tname>/*.cpp import=CppUnit import2=<timport>

#----------------------------------------------------------------------------
# Workarounds for LCGCMT issues on MacOSX
#----------------------------------------------------------------------------

# === Workaround in both CoolTest and CoolKernel ===
# Workaround for SPI-787 in LCG_os on mac109
macro LCG_os $(LCG_os) target-mac109 mac109

# === Workaround in both CoolTest and CoolKernel ===
# Workaround for SPI-787 for clang61 on mac1010
tag x86_64-mac1010-clang61-opt target-x86_64 target-mac1010 target-clang61 target-opt
tag target-clang61 target-c11
macro LCG_compiler $(LCG_compiler) target-clang61 "clang61"

# === Workaround in both CoolTest and CoolKernel ===
# Workaround for SPI-787 in Boost_compiler_version for clang on mac
macro Boost_compiler_version $(Boost_compiler_version) target-mac xgcc42

#============================================================================
# Private dependencies and build rules
#============================================================================

private

# Link the CORAL base libraries
apply_tag NEEDS_CORAL_BASE

use CppUnit v* LCG_Interfaces -no_auto_imports

# The unit tests
apply_pattern cool_unit_test tname=FolderSpec
apply_pattern cool_unit_test tname=Record
apply_pattern cool_unit_test tname=RecordAdapter
apply_pattern cool_unit_test tname=RecordSelection

# Fake target for utilities
action utilities "echo No utilities in this package"
macro_remove cmt_actions_constituents "utilities"

# Fake target for examples
action examples "echo No examples in this package"
macro_remove cmt_actions_constituents "examples"
