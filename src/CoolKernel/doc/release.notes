Package CoolKernel
Package managers: Andrea Valassi, Sven A. Schmidt and Marco Clemencic.

==============================================================================
!2008.11.10 - Andrea

Internal doc for tag COOL_2_6_0.

Summary of changes in CoolKernel with respect to COOL_2_6_0-pre6:
- Change API and implementation to replace 'const int f()' by 'int f()' 
  and get rid of gcc43 compilation warnings (bug #42584, aka task #8261).
- Reenable compilation warnings for gcc43 in all packages using CoolKernel.

==============================================================================
!2008.10.21 - Andrea

*** BRANCH RELEASE NOTES ***

Tag COOL_2_4_0b. Rebuild of the 2.4.0a release for the LCG_54h configuration.
Add support for Oracle on MacOSX. Bug fixes in CORAL and frontier_client.
No change in the source code. Software version remains "2.4.0".

NB: None of the _code_ changes in COOL_2_4-branch are released in COOL_2_4_0b!
NB: [there is one minor exception, RelationalCool/tests/RelationalDatabaseId]
NB: Only the _config_ branch changes are released in COOL_2_4_0b!

==============================================================================
!2008.10.16 - Andrea

Internal tag COOL_2_6_0-pre5-bis ('2008/10/16 10:00:00').

Changes in CoolKernel with respect to COOL_2_6_0-pre5:
- Performance optimization: issue COUNT(*) queries only on demand (task #7971).
  This implies a minor backward-incompatible change in the CoolKernel API: 
  remove const qualifier from IObjectIterator methods size, isEmpty, hasNext.
- Start the port to gcc4.3. 
  Set path to the compiler (workaround for bug #29124) as done for gcc4.1.
  Disable all warnings for the moment (hide the warnings from bug #42574).
  Add missing STL headers to the implementation code.
  Add gcc43 to all .cvsignore files.
- Define CORAL linkopts using the NEEDS_CORAL_BASE macro
  (fix for LCGCMT bug #41579: link RelationalAccess only if needed).

==============================================================================
!2008.09.26 - Andrea

Internal tag COOL_2_6_0-pre5.

No changes in CoolKernel with respect to COOL_2_6_0-pre4:

==============================================================================
!2008.09.25 - Andrea

Internal tag COOL_2_6_0-pre4.

Summary of changes in CoolKernel with respect to COOL_2_6_0-pre3:
- Fix preprocessor options passed to genreflex on Windows (bug #41380). 

==============================================================================
!2008.09.11 - Andrea

Internal tag COOL_2_6_0-pre3.

Summary of changes in CoolKernel with respect to COOL_2_6_0-pre2:
- Fix bug #36646 (metadata fields should not be visible in IObject::payload).
  Involves minor changes to ConstRecordAdapter API. Add a test.
- Add /DBOOST_LIB_DIAGNOSTIC and /DBOOST_ALL_NO_LIB for the vc9 build.

==============================================================================
!2008.09.01 - Andrea

Internal tag COOL_2_6_0-pre2.

Summary of changes in CoolKernel with respect to COOL_2_6_0-pre1:
- Move IApplication.h and MessageLevels.h to CoolKernel to remove 
  the cyclic CMT dependency between RelationalCool and CoolApplication.
- Change '#ifdef COOL260' into '#ifdef COOL270'.
- Disable ITransaction with #ifdef COOL270.
- Add IFolder::truncateObjectValidity (not implemented) behind #ifdef COOL270.

==============================================================================
!2008.08.27 - Andrea

Change '#ifdef COOL260' into '#ifdef COOL270'.

Some API extensions will definitely be released in COOL260
(tag cloning and a few changes to existing classes),
some may only make it to COOL270
(IOV truncation, payload queries, manual transations).

==============================================================================
!2008.08.27 - Andrea

Changes disabled by #ifdef COOL260 (truncateValidity, task #7656):
- Add the IFolder::truncateObjectValidity method.
  [This is not implemented yet!].
- This was previously commented out.

==============================================================================
!2008.08.27 - Andrea

Remove the cyclic CMT dependency between RelationalCool and CoolApplication.
- Move to CoolKernel the CoolApplication headers 
  IApplication.h and MessageLevels.h needed by RelationalCool.
- Remove the dependency of RelationalCool on CoolApplication.
  Keep only the dependency on CoolKernel.
- For backward compatibility of the public user API,
  keep in CoolApplication two files IApplication.h and MessageLevels.h 
  that simply include the real headers in CoolKernel.
- Adapt all implementation code and tests.

==============================================================================
!2008.08.27 - Andrea

Internal tag COOL_2_6_0-pre1.

Summary of changes in CoolKernel with respect to COOL_2_5_0 is the following.

Changes enabled by default:
- New functionalities in COOL260:
  > Add IField::cloneTagAsUserTag (task #4325)
  > No API changes are needed for partial tag locking (task #6053)
- Exception cleanup:
  > New class InternalErrorException
- Prepare for payload queries:
  > Move FieldSpecification copy constructor to public (was private)
  > Add method IField::setValue(const IField&) and a test
  > Add constructor Record(IFieldSpecification&) and a test
- Port to Windows VC9:
  > Avoid std::numeric_limits for Windows in ChannelSelection.h
  > Disable CRT deprecation warnings (bug #40144)
- Add cmt/version.cmt for the new CMT version.
- Remove SCRAM configuration fragments.

Changes disabled by #ifdef COOL260 (payload queries, task #2417)
- Add const IRecordSelection* argument to IFolder::browseObjects
- Add const IRecordSelection* argument to IFolder::countObjects
- New interface IRecordSelection
- New class RecordSelectionException, FieldSelection, CompositeSelection
- Add RecordSelection test

Changes disabled by #ifdef COOL260 (manual transactions, task #2200)
- New interface ITransaction
- Add method IDatabase::startTransaction

Changes commented out (truncateValidity, task #7656 - discussions ongoing)
- Add the IFolder::truncateObjectValidity method.
  [Had originally added FolderIsMultiVersion exception, then removed it].

==============================================================================
!2008.06.10 - Andrea

Tag COOL_2_5_0. Production release with API semantic changes, API extensions
and package structure changes to remove the dependency on SEAL.
Upgrade to LCG_55 using the 'de-SEALed' CORAL_2_0_0.

Changes in CoolKernel with respect to COOL_2_4_0a:
- No change in the public CoolKernel API.
- Replace seal::Time by cool::SealBaseTime.
- Drop CMT dependency on SEAL, add CMT dependency on Boost.

==============================================================================
!2008.06.04 - Andrea

*** BRANCH RELEASE NOTES ***

Tag COOL_2_4_0a. Rebuild of the 2.4.0 release for the LCG_54g configuration.
Port to osx105 and .so shared lib names on MacOSX. Changes in CORAL and ROOT.
No change in the source code. Software version remains "2.4.0".

NB: None of the _code_ changes in COOL_2_4-branch are released in COOL_2_4_0a!
NB: Only the (osx105) _config_ branch changes are released in COOL_2_4_0a!

==============================================================================
!2008.04.18 - Marco

Fixed bug #35709: Windows compile error (after SEAL removal).

[Changes relevant to 'de-SEALed' COOL only - not included in COOL_2_4-branch]

==============================================================================
!2008.04.09 - Andrea

Undo Marco's changes from 2008.03.20: go back to the COOL_2_4_0 API:
- Remove IDatabaseSvc::load and remove the Reflex dependency.
- Remove IDatabaseSvc::connectionSvc() - move it to IApplication instead.

[Changes relevant to 'de-SEALed' COOL only - not included in COOL_2_4-branch]

==============================================================================
!2008.04.09 - Marco/Andrea

Copied SealBase/Time.h (with minor changes) as new class cool::SealBaseTime to
drop the dependency on SEAL. Added a dependency on Boost (used in some tests).

[Changes relevant to 'de-SEALed' COOL only - not included in COOL_2_4-branch]

==============================================================================
!2008.04.02 - Andrea

Created a COOL_2_4 branch off COOL_2_4_1-pre1 for the whole of COOL:
  cvs rtag -r COOL_2_4_1-pre1 -b COOL_2_4-branch cool
For CoolKernel, COOL_2_4_1-pre1 is the same as COOL_2_4_0.

==============================================================================
!2008.03.20 - Marco

Added IDatabaseSvc::connectionSvc that returns the coral::ConnectionService
used in the Coral relational implementation (RalDatabaseSvc).

Added a static function cool::IDatabaseSvc::load that loads via Reflex 
the RalDatabaseSvc without the need of passing through cool::Application 
(but without the configuration steps that happen in cool::Application).
Added a dependency on Reflex to implement cool::IDatabaseSvc::load.

[Changes relevant to 'de-SEALed' COOL only - not included in COOL_2_4-branch]

==============================================================================
!2008.02.28 - Andrea

Tag COOL_2_4_0. Production release with backward compatible API extensions.
Upgrade to LCG_54b using CORAL_1_9_5.

==============================================================================
!2008.02.19 - Andrea (changes committed before COOL_2_3_1 but not tagged)

Add two API extensions to be released as COOL_2_4_0 (but not yet implemented):
- add IHvsNode::setTagDescription (task #6394), to allow users to first
  create a tag (also as a user tag or HVS tag) and then set its description
- add userTagOnly argument to IFolder::storeObject (task #6153), to allow
  users to avoid the duplication of MV data in the global and user tag HEADs

==============================================================================
!2008.02.21 - Andrea

Tag COOL_2_3_1. Bug-fix production release (binary compatible with 2.3.0) 
with performance optimizations for multi-version retrieval and insertion.
Upgrade to LCG_54a including bug fixes in ROOT 5.18.00a. 

==============================================================================
!2008.02.13 - Andrea (-D '2008/02/13 19:30:00')

Internal tag COOL_2_3_1-pre2 (bug fixes and performance optimizations).
Use the default LCG_54 (including ROOT 5.18) on all platforms.

==============================================================================
!2008.02.04 - Andrea

Internal tag COOL_2_3_1-pre1 (bug fixes and performance optimizations).

==============================================================================
!2008.01.21 - Andrea

Tag COOL_2_3_0. Production release with backward compatible API extensions.
Upgrade to LCG_54 using Python 2.5, ROOT 5.18 and several other new externals.

PyCool is not supported on MacOSX because of bug #32770 in ROOT 5.18.00.
The COOL nightlies are all green (except for the PyCool tests on MacOSX).

==============================================================================
!2007.01.14 - Andrea

Move method Record::extend(const Record&) to the public API (was private),
allowing users to merge the specification and data of two existing Records. 

This is very useful for building more modular query definitions in various
performance optimization issues (e.g. task #6086).

NB: I have not added explicit tests for Record::extend(const Record&).

==============================================================================
!2007.01.09 - Andrea

New signature (record) for IFolder::extendPayloadSpecification (task #5742).
This allows users to add more than one columns at once and to specify 
a non-null default value for the additional column of all existing rows. 
Remove the old signature (name, type).

==============================================================================
!2007.12.19 - Andrea

Internal tag COOL_2_3_0-pre1 (new COOL API extensions on standard LCG_53f).
All CMT tests successful, bash tests not updated. Software version is 2.3.0.

==============================================================================
!2007.12.13 - Andrea

Added enum value HvsTagLock::PARTIALLYLOCKED (task #4606).

A PARTIALLYLOCKED tag behaves like a LOCKED tag except in the following cases:
1. It is possible to add a tag relation from a partially locked parent HVS tag
to a tag in a child HVS node (if no tag relation exists for that child node).

Eventually, one or both of the following functionalities may also be added
(but for the moment, neither of them is implemented yet):
2. It is possible to add IOVs to a partially locked user tag, only if the 
newly inserted IOVS have no overlap to those already existing in the user tag.
3. It is possible to retag the HEAD of a folder with a partially locked tag,
only if the retag operation only results in adding new IOVS to the tag.

==============================================================================
!2007.12.13 - Andrea

Added method IFolder::listChannelsWithNames (task #5868).

Aborted attempts to add method IObject::channelName. The functionality 
required by Atlas is provided by IFolder::listChannelsWithNames instead.

==============================================================================
!2007.12.12 - Andrea

Added method IFolder::extendPayloadSpecification (task #5742).

==============================================================================
!2007.12.12 - Andrea

Added exception DatabaseOpenInReadOnlyMode to the public API (task #5743).
This was previously defined as a RelationalException in RelationalCool.

==============================================================================
!2007.11.26 - Sven

Added channel selection by channel name in the ChannelSelection class.

==============================================================================
!2007.11.13 - Andrea

Tag COOL_2_2_2. Production release (binary compatible with 2.2.0) with many
performance and configuration improvements and bug fixes. New versions of 
CORAL and Frontier server (fixing all pending problems in the tests) and ROOT.
This is the first COOL release built by the SPI team (and no SCRAM config).

==============================================================================
!2007.11.08 - Andrea

Internal tag COOL_2_2_2-pre2 (using private CORAL192 build and SEAL193 copy).
All CMT tests successful, bash tests not updated. Software version is 2.2.2.

==============================================================================
!2007.11.07 - Andrea

Internal tag COOL_2_2_2-pre1 (using private CORAL192 build and SEAL193 copy).
All CMT tests successful (no pending Frontier failures).
Bash tests not updated (Wine failures are expected).

==============================================================================
!2007.10.30 - Andrea

Remove old workaround for bug #28037 (CMTEXTRATAGS=SKIP_INSTALL_INCLUDES).

==============================================================================
!2007.10.30 - Andrea

Added a fake make target "examples" to please nmake (task #5414).

==============================================================================
!2007.10.11 - Andrea

Tag COOL_2_2_1. Production release (binary compatible with 2.2.0) with many
configuration improvements, feature enhancements and bug fixes. New versions 
of CORAL (with important bug fixes for SQLite and Frontier), ROOT and SEAL.
This is the first COOL release with support for MacOSX/Intel platforms.

==============================================================================
!2007.10.09 - Andrea

Drop SEAL package granularity in SCRAM as there is no .SCRAM in SEAL_1_9_3.
CoolKernel depends on SEAL and adds link libraries SealBase, PluginManager 
and SealKernel by default. SealUtil and SealServices are added where needed.

==============================================================================
!2007.10.08 - Andrea

Internal tag COOL_2_2_1-pre5. Last private tag before COOL_2_2_1.
All CMT/SCRAM tests successful on all platforms using a private CORAL191
(except for the last pending failure RO_02b1 - bug #23368 in FrontierAccess),
including MacOSX/PPC (last build) and gcc41 (not built for official COOL221).
Non-debug osx104_ia32_gcc401 not built, test results copied from debug version.

No changes in CoolKernel code (except for a comment) or config.

==============================================================================
!2007.08.29 - Andrea (-D '2007/08/29 13:30:00')

Internal tag COOL_2_2_1-pre4.
First version with successful tests of slc4_ia32_gcc41 on SCRAM/BASH,
including SQLite and Frontier support and complete private config for CMT.

No changes in CoolKernel code or config.

==============================================================================
!2007.08.24 - Andrea (-D '2007/08/24 17:15:00')

Internal tag COOL_2_2_1-pre3.
First version with successful tests of MacOSX Intel on both CMT/QMTEST 
and SCRAM/BASH, including PyCool (but still no Oracle support).
First version with successful tests of slc4_ia32_gcc41 on SCRAM/BASH
(but CMT is badly configured, and still no SQLite or Frontier support).

Minor fix in SCRAM BuildFile.
Add a workaround for bug in Foundation/SealPlatform/BuildFile, which exports 
"INCLUDE += $(SEAL)/$(SCRAM_ARCH)/include" even if SEAL is not defined.

Fix a comment in IObjectIterator.h about the usage of iterators.

==============================================================================
!2007.08.07 - Andrea (-D '2007/08/07 16:02:00')

Internal tag COOL_2_2_1-pre2.
First version with all QMTEST tests as successful as bash tests.

CMT config improvements:
- New workaround for bug #28037 (install includes on a single platform).
  In all packages with public headers (CoolKernel and CoolApplication),
  change the constituents macro so that the installation of includes is
  skipped in builds using 'cmt make CMTEXTRATAGS=SKIP_INSTALL_INCLUDES'.
- Remove the previous workaround for bug #28037 in CoolKernel.
  Remove the private install_includes_header fragment in CoolKernel.

No changes in CoolKernel code.

==============================================================================
!2007.08.02 - Andrea (-D '2007/08/03 14:05:00')

Internal tag COOL_2_2_1-pre1. 

CMT config improvements:
- Workaround for bug #28037 (install includes on a single platform).
  Define a private install_includes_header fragment in CoolKernel, such that 
  the installation is not executed by 'cmt make' or 'cmt make all_groups',
  but must be triggered manually using 'cmt make installincludes_install'.
- Fixes for OSX link warnings.

No changes in CoolKernel code.

==============================================================================
!2007.07.13 - Andrea

Tag COOL_2_2_0. Production release with many performance optimizations and bug 
fixes, also including backward-compatible API extensions and schema changes.
New versions of CMT, CORAL, ROOT/Reflex, oracle, sqlite, frontier_client
and LFC using LCG_53 (with respect to COOL 2.1.1 using LCG_51).

Main API changes in CoolKernel:
- Added support for Blob16M (task #2197).
- Added reverse order object iterator from browseObjects (task #3430).
- Added goToNext() and currentRef() methods to IObjectIterator to improve the 
  C++ client performance by avoiding copies of CORAL currentRow (task #4672).
- Removed unnecessary virtual class inheritance from final classes and 
  unnecessary virtual method inheritance from final methods (task #4879).
- Added IDatabase::databaseName() method (sr #101807).
- Added IFolder::dropChannel() method to delete/drop an existing channel.

==============================================================================
!2007.04.16 - Marco

Tag COOL_2_1_1. Bug-fix production release (binary compatible with 2.1.0).
New versions of CORAL, ROOT and frontier_client using LCGCMT_51.

Changes in CoolKernel API:
- Added goToNext() and currentRef() methods to IObjectIterator.
  These are presently disabled (#ifdef'ed out): they will be enabled in 2.2.0 
  (improve C++ client performance by avoiding copies of CORAL currentRow).

==============================================================================
!2007.03.24 - Andrea

Tag COOL_2_1_0. Production release with backward-compatible API extensions 
and schema changes. New versions of CORAL and ROOT using LCGCMT_50c.

Summary of main API changes in CoolKernel:
- Tag locking functionality (task #3709)
  > add IHvsTag::setTagLockStatus and IHvs::tagLockStatus methods
  > add TagIsLocked exception
- Fix for bug #23751 in the IFolder::folderSpecification() method.
- Fixes to allow compilation on MacOSX.
- Define int64 as long long also on AMD64, as in CORAL_1_7_2 (task #4357).

==============================================================================
!2007.03.20 - Andrea

Add tag locking functionality (task #3709): add IHvsTag::setTagLockStatus 
and IHvs::tagLockStatus methods, and TagIsLocked exception.

==============================================================================
!2007.02.21 - Andrea

Added TestListener.h replacing the corresponding cppunit header file.
This seemed to be the only way to get rid of compilation warnings with -O2.

==============================================================================
!2007.02.14 - Marco

Fixed bug #23751: "IFolder::folderSpecification() does not work".

The fix required a fix in IFolder.h (return const IRecordSpecification instead
of const RecordSpecification). It is "semantically" compatible, and should not
break compilation, but it is not strictly the same API.

==============================================================================
!2007.02.07 - Andrea

Fixes for MacOSX in types.h min/max constant definitions.

==============================================================================
!2007.01.31 - Andrea

Tag COOL_2_0_0. Major production release with backward-incompatible
API and schema changes. New versions of SEAL, CORAL and frontier_client. 

Summary of main API changes in CoolKernel and CoolApplication:
- Changes in folder specification, payload specification and payload data API
  > new StorageType::TypeId enum with associated new typedefs (eg Int64)
  > new RecordSpecification class
  > new Record and Field classes with validation of storage constraints
  > temporary extensions use coral AttributeList for backward compatibility 
  > new FolderSpecification class accepted by createFolder
  > treat empty string "" as NULL
- Stripped API of all SEAL classes
  > integer type definitions not based on seal::IntBits
  > new class cool::Time replaces seal::Time.
  > new SEAL-free API for class cool::Application
- Changes in the support for user-defined payload fields
  > added support for BLOB and signed int64 payload data
  > constraints on numbers and names of user-defined payload fields
  > added cool::IFolder::renamePayload (to change unsupported payload names)
  > string payload cannot contain character "\0" (only BLOBs can contain it)
- Renamed methods and removed default channel arguments in IFolder:
- Removed obsolete IDatabaseSvc::dropDatabase(dbId,throwIfDoesNotExist)
- Added the possibility of specifying a CORAL role in the connection string
- Changed default mode for openDatabase from read/write to read/only

==============================================================================
!2006.01.18 - Andrea

Get rid of SEAL from the public CoolKernel API.

Use native C++ types (with one #define for LONG_LONG_MAX on Win) for the 
16/32/64 bit integer types. Note that long long is supported on Win as of VC71.

Hide seal::Time behind a COOL-specific simple ITime interface and Time class.
The SEAL-based implementation can be removed (eg replaced by boost) without
changing the public CoolKernel headers (only the implementation would change).

==============================================================================
!2007.01.17 - Marco

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_4. Production release (backward-compatible bug-fix, 
functionality enhancement and LCG_50 configuration upgrade release). 
Important fixes in SEAL (component model and multithreading), CORAL and ROOT.

==============================================================================
!2006.12.10 - Andrea

Remove obsolete IDatabaseSvc::dropDatabase( dbId, throwIfDoesNotExist ).

==============================================================================
!2006.12.07 - Andrea

Completed internal port to new IRecord and IField API.

Eased backward compatibility for users by keeping the old 
storeObject(..AttributeList&..) method signature in parallel 
to the new storeObject(..IRecord&..), for writing,
and via the IRecord::attributeList()method, for reading.

PyCool still uses only the AttributeList interface
(Marco will port it to the new Record interface).

All C++ and python tests pass.

==============================================================================
!2006.11.08 - Andrea

Tidy up type definitions across CoolKernel header files and tests.

Rename Short and UShort as Int16 and UInt16.
Define them using seal::IntBits<16> instead of short and unsigned short.

Move all type definitions from StorageType.h to types.h.
Remove type definitions for sIntXX/uIntXX in types.h.
These are now replaced by IntXX and UIntXX (coming from StorageType.h).

Base the definition of ValidityKey on the UInt63 type.
Base the definition of ChannelId on the Int32 type.

Add definition of min/max values for Int16/UInt16 (short/unsigned short).
Use this definition in the tests instead of SHRT_MIN and SHRT_MAX
(and do the same for the min/max of the 32 and 64 bit integer types).

Move the AttributeListSpecification releaser to the private headers
of package RelationalCool (it is only used by a couple of tests there).

==============================================================================
!2006.10.30 - Andrea

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_3c. Rebuild of the 1.3.3 release for the LCG_48 configuration.
Important fixes in SEAL (component model and multithreading), CORAL and ROOT.
No change in the source code. Software version remains "1.3.3".

==============================================================================
!2006.10.16 - Marco

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_3b. Rebuild of the 1.3.3 release for the LCG_47b configuration.
New version of ROOT. Pending bugs in SEAL and ROOT. 
Partial support for MySQL on Windows (pending PyCoolUtilities bug #20780). 
No change in the source code. Software version remains "1.3.3".

===============================================================================
!2006.10.25 - Marco

Fixed a warning with gcc 3.4 (base class `class cool::IRecordSpecification' 
should be explicitly initialized in the copy constructor).

===============================================================================
!2006.10.09 - Marco

Added support (and tests) for coral::Blob.

===============================================================================
!2006.09.29 - Andrea

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_3a. Rebuild release (bug fixes in CORAL and frontier_client).
Pending bugs in SEAL, new bugs in ROOT. Same source code as COOL_1_3_3. 
Only added two Frontier regression tests. Software version remains "1.3.3".

=============================================================================
!2006.08.28 - Andrea

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_3. Production release (backward-compatible bug-fix, 
functionality enhancement and configuration upgrade release).
Many important fixes in CORAL and Frontier; pending critical bugs in SEAL.

==============================================================================
!2006.07.14 - Marco

Added function cool::IFolder::renamePayloadColumn and tests (task #3609).

==============================================================================
!2006.07.12 - Marco

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_2c. Rebuild of COOL_1_3_2 for the LCG_46 configuration.
No change in the source code. Software version remains "1.3.2".

==============================================================================
!2006.06.19 - Andrea

Changed default openDatabase from read/write to read/only.
Backward incompatible API change (to be released in COOL200).
May require several changes in the tests.

==============================================================================
!2006.06.19 - Marco

*** BRANCH RELEASE NOTES ***

Tag COOL_1_3_2b. Rebuild of COOL_1_3_2 for the LCG_45 configuration.
No change in the source code. Software version remains "1.3.2".

==============================================================================
!2006.06.16 - Marco

Added support for Int64 (UInt64 is ready, but commented out).

[Andrea: signed int64 was not supported so far because the OCI-22053 error 
was observed for Oracle in retrieving LONG_LONG_MIN (-9223372036854775808).
Marco cross-checked that this error is not observed anymore on Linux.
It was later verified that the problem disappears thanks to a bug fix
in the Oracle client library 10.2 (while it was present until 10.1).
In November 2006, the problem is still pending for OSX because the
Oracle 10.2 client for OSX has not been released yet (see bug #21949).
Support for Int64 can safely be reenabled as the pending OCI-22053 is a 
bug in the Oracle client on a platform that is not yet supported by COOL.]

==============================================================================
!2006.05.14 - Andrea

Tag COOL_1_3_2a. Rebuild of the 1.3.2 release for the LCG_44 configuration.

==============================================================================
!2006.05.10 - Andrea

Tag COOL_1_3_2. Production release (backward-compatible 
bug-fix and Frontier support release in the 1.3 series).

==============================================================================
!2006.04.25 - Andrea, Marco, Sven

Tag COOL_1_3_1. Production release (backward-compatible 
COOL and CORAL bug-fix release in the 1.3 series).

==============================================================================
!2006.04.04 - Andrea

Tag COOL_1_3_0. Functionality enhancement production release (first 
release in the 1.3 series: backward incompatible API and schema changes).

Summary of API changes:
- HVS functionality
  > new IHvsNode methods: create/delete/findTagRelation and resolveTag
  > new HVS exceptions Node/Tag[Relation]NotFound and NodeIsSingleVersion
  > split IHvsNodeRecord from IHvsNode 
- prepare for further HVS features (reuse same tag name for several folders)
  > changed std::string tagScope() to IHvsNode::Type tagNameScope()
  > added std::vector<std::string> taggedNodes()
- CORAL connection service (URLs can be specified using aliases)
- user tags (storeObject accepts an optional tag name argument)
  > added existsUserTag()
- drop all POOL dependencies (API now based on coral::AttributeList)
- cool::Exception derived from std::exception instead of seal::Exception
- const qualifyers added to many methods (not yet complete)

==============================================================================
!2006.03.03 - Andrea

Backward-incompatible API change.

Drop obsolete method dropNode( nodeName, throwIfDoesNotExist ).

==============================================================================
!2006.03.03 - Andrea

Backward-incompatible API change (start adding HVS functionality).

Change the semantics of the tagScope method in IDatabase: it now returns 
whether a tag can be used for inner nodes (folder sets) or leaf nodes
(folders), rather than returning the node name it is used for. In HVS 
(following a request from LHCb), the same tag name can be used for only 
one folder set or for several folders. There is not anymore a unique node
each tag can be attached to. Also, a tag can be reserved for inner nodes
or leaf nodes without being actually attached to any specific nodes yet.

==============================================================================
!2006.03.08 - Marco

*** BRANCH RELEASE NOTES ***

Tag COOL_1_2_9 (non-HEAD branch after COOL_1_2_8).
Backward-compatible production release.
Same code as 1.2.8, but compiled against LCG_42_4.

==============================================================================
!2006.01.27 - Andrea

Tag COOL_1_2_8. Backward-compatible production release (internal migration 
from SEAL Reflex to ROOT Reflex; port to gcc344; attempted port to AMD64).

==============================================================================
!2006.01.16 - Andrea

Tag COOL_1_2_7. Backward-compatible production release
(internal migration from RAL to CORAL and from Reflection to seal Reflex).

==============================================================================
!2005.11.15 - Marco

Tag COOL_1_2_6. Production release (backward-compatible 
SEAL_1_7_6 and POOL_2_2_4 upgrade release in the 1.2 series).

==============================================================================
!2005.10.24 - Andrea, Marco

Tag COOL_1_2_5. Production release (backward-compatible 
SEAL_1_7_5 and POOL_2_2_3 upgrade release in the 1.2 series).

Retag COOL_1_2_4 code as COOL_1_2_5. 

==============================================================================
!2005.09.29 - Andrea, Sven, Marco, David, Uli

Tag COOL_1_2_4. Production release (backward-compatible 
bug-fix and POOL_2_2_1 upgrade release in the 1.2 series).

==============================================================================
!2005.08.29 - Andrea, Sven and Marco

Tag COOL_1_2_3. Production release (backward-compatible 
API enhancement and bug-fix release in the 1.2 series).

==============================================================================
!2005.07.27 - Andrea and Sven

Tag COOL_1_2_2. Production release (upgrade to SEAL_1_7_1 bug fix release).

==============================================================================
!2005.07.20 - Andrea and Sven

Tag COOL_1_2_1. Production release (backward-compatible 
API enhancement and bug-fix release in the 1.2 series).

==============================================================================
!2005.07.07 - Andrea and Sven

Internal tags for the next production release.

Tag COOL_1_2_1-pre1.

==============================================================================
!2005.06.28 - Andrea and Sven

Tag COOL_1_2_0. Production release (first release in the 1.2 series:
backward-incompatible API and schema changes).

==============================================================================
!2005.05.25 - Andrea and Sven

Tag COOL_1_1_0. Production release (first release in the 1.1 series:
repackaging and backward-incompatible API changes).

==============================================================================
!2005.05.09 - Andrea and Sven

Tag COOL_1_0_2. Production release (backward-compatible 
API enhancement and bug-fix release in the 1.0 series).

First production release using the new COOL CVS repository.

==============================================================================
!2005.05.02 - Andrea and Sven

Tag COOL_1_0_2-pre1. 

Internal release: same code as COOL_1_0_1, using the new COOL CVS repository.

==============================================================================
!2005.04.25 - Andrea and Sven

Tag COOL_1_0_1. Production release (bug fix release in the 1.0 series).

==============================================================================
!2005.04.21 - Andrea and Sven

Tag COOL_1_0_0. FIRST PRODUCTION RELEASE!

==============================================================================
!2005.04.21 - Andrea and Sven

Internal pre-release tags for the production release.

Tag COOL_1_0_0-pre3.
Tag COOL_1_0_0-pre2.
Tag COOL_1_0_0-pre1.

==============================================================================
!2005.04.09 - Andrea and Sven

Intermediate changes towards the production release.

==============================================================================
!2005.03.20 - Andrea and Sven

Intermediate tags towards the production release.

Tag COOL_0_2_0-pre1 (just replace seal::LongLong by seal::IntBits<64>::SLeast).
Tag COOL_0_2_0-pre2 (just rename IValidityKey and IChannelId without the "I").
Tag COOL_0_2_0-pre3 (store ValidityKey's as signed int64).
Tag COOL_0_2_0-pre4 (define ValidityKeyMin/Max as LONG_LONG_MIN/MAX).

==============================================================================
!2005.03.19 - Andrea and Sven

Intermediate tags towards the production release.
More details can be found in the RelationalCool release notes.

Tag COOL_0_1_1-pre1 (many bug fixes and a few extra functionalities).
Tag COOL_0_1_1-pre2 (fixed all unit tests).

==============================================================================
!2005.03.03 - Andrea and Sven

First complete prototype with the versioning and tagging functionalities.
Tagged as prerelease COOL_0_1_0-pre1.
Tagged as prerelease COOL_0_1_0-pre2 (only configuration changes).

==============================================================================
!2005.02.02 - Andrea and Sven

First version made publicly available for feedback.
Tagged as prerelease COOL_0_0_3-pre1.
Tagged as prerelease COOL_0_0_3-pre2 with Windows support added.

==============================================================================
!2004.11.09 - Andrea and Sven 

New package. 

This package contains the abstract API of the COOL kernel component.
It is the equivalent of the ConditionsDB package in the old common API.

==============================================================================
