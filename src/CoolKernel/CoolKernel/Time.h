#ifndef COOLKERNEL_TIME_H
#define COOLKERNEL_TIME_H 1

// Include files
#include "CoralBase/TimeStamp.h" // NB: CORAL3 TimeStamp is needed!
#include "CoolKernel/ITime.h"

namespace cool
{

  /** @class Time Time.h
   *
   *  Simple COOL time class.
   *
   *  Basic sanity checks on value ranges are delegated in the internal
   *  implementation to a more complex Time class (e.g. coral::TimeStamp).
   *
   *  @author Andrea Valassi
   *  @date   2007-01-17
   *///

  class Time : public ITime
  {

  public:

    /// Destructor.
    virtual ~Time();

    /// Default constructor: returns the current UTC time.
    Time();

    /// Constructor from an explicit date/time - interpreted as a UTC time.
    /// Sanity checks on the ranges of the input arguments are implemented.
    Time( int year,
          int month,
          int day,
          int hour,
          int minute,
          int second,
          long nanosecond );

    /// Copy constructor from another Time.
    Time( const Time& rhs );

    /// Assignment operator from another Time.
    Time& operator=( const Time& rhs );

    /// Copy constructor from any other ITime.
    Time( const ITime& rhs );

    /// Assignment operator from any other ITime.
    Time& operator=( const ITime& rhs );

    /// Copy constructor from a coral::TimeStamp.
    Time( const coral::TimeStamp& rhs );

    /// Assignment operator from a coral::TimeStamp.
    Time& operator=( const coral::TimeStamp& rhs );

    /// Return the underlying coral::TimeStamp.
    const coral::TimeStamp& coralTimeStamp() const
    {
      return m_ts;
    }

    /// Returns the year.
    int year() const
    {
      return m_ts.year();
    }

    /// Returns the month [1-12].
    int month() const
    {
      return m_ts.month();
    }

    /// Returns the day [1-31].
    int day() const
    {
      return m_ts.day();
    }

    /// Returns the hour [0-23].
    int hour() const
    {
      return m_ts.hour();
    }

    /// Returns the minute [0-59].
    int minute() const
    {
      return m_ts.minute();
    }

    /// Returns the second [0-59].
    int second() const
    {
      return m_ts.second();
    }

    /// Returns the nanosecond [0-999999999].
    long nanosecond() const
    {
      return m_ts.nanosecond();
    }

    /// Print to an output stream.
    std::ostream& print( std::ostream& os ) const;

    /// Comparison operator.
    bool operator==( const ITime& rhs ) const;

    /// Comparison operator.
    bool operator>( const ITime& rhs ) const;

  private:

    coral::TimeStamp m_ts;

  };

}

#endif // COOLKERNEL_TIME_H
