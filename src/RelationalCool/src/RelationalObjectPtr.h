#ifndef RELATIONALCOOL_RELATIONALOBJECTPTR_H
#define RELATIONALCOOL_RELATIONALOBJECTPTR_H 1

// Include files
#include <memory>

namespace cool
{

  // Forward declarations
  class RelationalObject;

  /// Shared pointer to a RelationalObject
  typedef std::shared_ptr<RelationalObject> RelationalObjectPtr;

}

#endif
