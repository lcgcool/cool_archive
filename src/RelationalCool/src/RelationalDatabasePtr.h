#ifndef RELATIONALCOOL_RELATIONALDATABASEPTR_H
#define RELATIONALCOOL_RELATIONALDATABASEPTR_H

// Include files
#include <memory>

namespace cool
{

  // Forward declarations
  class RelationalDatabase;

  /// Shared pointer to a RelationalObject
  typedef std::shared_ptr<RelationalDatabase> RelationalDatabasePtr;

}

#endif
